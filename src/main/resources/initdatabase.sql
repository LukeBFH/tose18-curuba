
create table if not exists Mitarbeiter(id LONG auto_increment PRIMARY KEY, funktion VARCHAR(50) NOT NULL, Abteilung VARCHAR(50), hierarchiestufe VARCHAR(20), lohnstufe INT, name VARCHAR(60) NOT NULL, nachname VARCHAR(60) NOT NULL, adresse VARCHAR(100), plz INT, ort VARCHAR(100), mail VARCHAR(100), tel INT);


create table if not exists Projekt(id LONG auto_increment PRIMARY KEY, name VARCHAR(200) NOT NULL, beschreibung VARCHAR(200), relevanz VARCHAR(200), kostenSOLL INTEGER, kostenIST INTEGER);


create table if not exists Risiko(id LONG  auto_increment PRIMARY KEY, Risikort VARCHAR(50), beschreibung VARCHAR(200), ewk INT, asm INT, massnahmen VARCHAR(100), status VARCHAR(1),verantwortlich VARCHAR(50));


create table if not exists Stakeholder(id LONG auto_increment PRIMARY KEY, name VARCHAR(50), vorname VARCHAR(200), anforderung VARCHAR(100), einfluss VARCHAR(10), konfliktpotenzial INT, rolle VARCHAR(50));
