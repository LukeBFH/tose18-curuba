package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.bfh.sparklist.web.MitarbeiterDeleteController;
import ch.briggen.bfh.sparklist.web.MitarbeiterEditController;
import ch.briggen.bfh.sparklist.web.MitarbeiterNewController;
import ch.briggen.bfh.sparklist.web.MitarbeiterUpdateController;
import ch.briggen.bfh.sparklist.web.ProjektDeleteController;
import ch.briggen.bfh.sparklist.web.ProjektEditController;
import ch.briggen.bfh.sparklist.web.ProjektNewController;
import ch.briggen.bfh.sparklist.web.ProjektUpdateController;
import ch.briggen.bfh.sparklist.web.RisikoDeleteController;
import ch.briggen.bfh.sparklist.web.RisikoEditController;
import ch.briggen.bfh.sparklist.web.RisikoNewController;
import ch.briggen.bfh.sparklist.web.RisikoUpdateController;
import ch.briggen.bfh.sparklist.web.StakeholderDeleteController;
import ch.briggen.bfh.sparklist.web.StakeholderEditController;
import ch.briggen.bfh.sparklist.web.StakeholderNewController;
import ch.briggen.bfh.sparklist.web.StakeholderUpdateController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
		
	//MitarbeiterControllers
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/Mitarbeiter", new MitarbeiterEditController(), new UTF8ThymeleafTemplateEngine());
		post("/Mitarbeiter/update", new MitarbeiterUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/Mitarbeiter/delete", new MitarbeiterDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/Mitarbeiter/new", new MitarbeiterNewController(), new UTF8ThymeleafTemplateEngine());
		
	//ProjektControllers
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/Projekt", new ProjektEditController(), new UTF8ThymeleafTemplateEngine());
		post("/Projekt/update", new ProjektUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/Projekt/delete", new ProjektDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/Projekt/new", new ProjektNewController(), new UTF8ThymeleafTemplateEngine());
		
	//RisikoControllers
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/Risiko", new RisikoEditController(), new UTF8ThymeleafTemplateEngine());
		post("/Risiko/update", new RisikoUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/Risiko/delete", new RisikoDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/Risiko/new", new RisikoNewController(), new UTF8ThymeleafTemplateEngine());
		
	//StakeholderControllers
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		get("/Stakeholder", new StakeholderEditController(), new UTF8ThymeleafTemplateEngine());
		post("/Stakeholder/update", new StakeholderUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/Stakeholder/delete", new StakeholderDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/Stakeholder/new", new StakeholderNewController(), new UTF8ThymeleafTemplateEngine());	
		
		

	}

}
