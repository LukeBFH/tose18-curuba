package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Mitarbeiter
 * @author nathanael
 */

public class StakeholderNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderNewController.class);
		
	private StakeholderRepository StakeholderRepo = new StakeholderRepository();
	
	/**
	 * Erstellt einen neuen Stakeholder in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Stakeholder&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /Stakeholder/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Stakeholder StakeholderDetail = StakeholderWebHelper.StakeholderFromWeb(request);
		log.trace("POST /Stakeholder/new mit StakeholderDetail " + StakeholderDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = StakeholderRepo.insert(StakeholderDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /Stakeholder?id=432932
		response.redirect("/Stakeholder?id="+id);
		return null;
	}
}


