package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Risiko;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * @author nathanael
 */

public class RisikoUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(RisikoUpdateController.class);
		
	private RisikoRepository RisikoRepo = new RisikoRepository();
	


	/**
	 * Schreibt das geänderte Risiko zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Risiko) mit der Risiko-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /Risiko/update
	 * 
	 * @return redirect nach /Risiko: via Browser wird /Risiko aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Risiko RisikoDetail = RisikoWebHelper.RisikoFromWeb(request);
		
		log.trace("POST /Risiko/update mit RisikoDetail " + RisikoDetail);
		
		//Speichern des Risikos in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /Risiko&id=3 (wenn RisikoDetail.getId == 3 war)
		RisikoRepo.save(RisikoDetail);
		response.redirect("/Risikoid="+RisikoDetail.getId());
		return null;
	}
}


