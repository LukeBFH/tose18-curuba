package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Projekt;
import spark.Request;


class ProjektWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(ProjektWebHelper.class);
	
	public static Projekt ProjektFromWeb(Request request)
	{
		return new Projekt(
				Long.parseLong(request.queryParams("ProjektDetail.id")),
				request.queryParams("ProjektDetail.name"),
				
				null, null, Integer.parseInt(request.queryParams("itemDetail.quantity")), 0);
	}

}
