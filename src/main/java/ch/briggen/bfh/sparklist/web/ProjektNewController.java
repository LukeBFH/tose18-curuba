package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projekte
 * @author nathanael
 */

public class ProjektNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektNewController.class);
		
	private ProjektRepository ProjektRepo = new ProjektRepository();
	
	/**
	 * Erstellt ein neues Projekt in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Projekt&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /Projekt/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projekt ProjektDetail = ProjektWebHelper.ProjektFromWeb(request);
		log.trace("POST /Projekt/new mit ProjektDetail " + ProjektDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = ProjektRepo.insert(ProjektDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /Projekt?id=432932
		response.redirect("/Projekt?id="+id);
		return null;
	}
}


