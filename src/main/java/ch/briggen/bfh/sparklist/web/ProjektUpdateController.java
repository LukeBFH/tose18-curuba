package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projekte
 * @author nathanael
 */

public class ProjektUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(ProjektUpdateController.class);
		
	private ProjektRepository ProjektRepo = new ProjektRepository();
	


	/**
	 * Schreibt das geänderte Projekt zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Projekt) mit der Projekt-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /Projekt/update
	 * 
	 * @return redirect nach /Projekt: via Browser wird /Projekt aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Projekt ProjektDetail = ProjektWebHelper.ProjektFromWeb(request);
		
		log.trace("POST /Projekt/update mit ProjektDetail " + ProjektDetail);
		
		//Speichern des Projekts in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /Projekt&id=3 (wenn ProjektDetail.getId == 3 war)
		ProjektRepo.save(ProjektDetail);
		response.redirect("/Projektid="+ProjektDetail.getId());
		return null;
	}
}


