package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * @author nathanael
 *
 */

public class MitarbeiterDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterDeleteController.class);
		
	private MitarbeiterRepository MitarbeiterRepo = new MitarbeiterRepository();

	/**
	 * Löscht den Mitarbeiter mit der übergebenen id in der Datenbank
	 * @return Redirect zurück zur Liste
	 */	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /Mitarbeiter/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		MitarbeiterRepo.delete(longId);
		response.redirect("/Mitarbeiter");
		return null;
	}
}


