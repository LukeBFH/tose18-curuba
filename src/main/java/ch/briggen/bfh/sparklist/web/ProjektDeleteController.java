package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Projekte
 * @author nathanael
 *
 */

public class ProjektDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(ProjektDeleteController.class);
		
	private ProjektRepository ProjektRepo = new ProjektRepository();

	/**
	 * Löscht das Projekt mit der übergebenen id in der Datenbank
	 * @return Redirect zurück zur Liste
	 */	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /Projekt/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		ProjektRepo.delete(longId);
		response.redirect("/Projekt");
		return null;
	}
}


