package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



import ch.briggen.bfh.sparklist.domain.Risiko;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * @author nathanael
 */

public class RisikoNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(RisikoNewController.class);
		
	private RisikoRepository RisikoRepo = new RisikoRepository();
	
	/**
	 * Erstellt ein neues Risiko in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Risiko&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /Risiko/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Risiko RisikoDetail = RisikoWebHelper.RisikoFromWeb(request);
		log.trace("POST /Risiko/new mit RisikoDetail " + RisikoDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = RisikoRepo.insert(RisikoDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /Risiko?id=432932
		response.redirect("/Risiko?id="+id);
		return null;
	}
}


