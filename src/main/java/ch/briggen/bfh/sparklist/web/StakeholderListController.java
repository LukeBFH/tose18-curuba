package ch.briggen.bfh.sparklist.web;

import java.util.Collection;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

public class StakeholderListController  implements TemplateViewRoute{
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(StakeholderManagementRootController.class);

	StakeholderRepository repository = new StakeholderRepository();

	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		//Items werden geladen und die Collection dann für das Template unter dem namen "Stakeholder" bereitgestellt
		//Das Template muss dann auch den Namen "Stakeholder" verwenden.
		HashMap<String, Collection<Stakeholder>> model = new HashMap<String, Collection<Stakeholder>>();
		model.put("Stakeholder", repository.getAll());
		return new ModelAndView(model, "listStakeholderTemplate");
	}
}
