package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Mitarbeiter
 * @author nathanael
 */

public class MitarbeiterNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterNewController.class);
		
	private MitarbeiterRepository MitarbeiterRepo = new MitarbeiterRepository();
	
	/**
	 * Erstellt einen neuen Mitarbeiter in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /Mitarbeiter&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /Mitarbeiter/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter MitarbeiterDetail = MitarbeiterWebHelper.MitarbeiterFromWeb(request);
		log.trace("POST /Projekt/new mit ProjektDetail " + MitarbeiterDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long id = MitarbeiterRepo.insert(MitarbeiterDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /Mitarbeiter?id=432932
		response.redirect("/Mitarbeiter?id="+id);
		return null;
	}
}


