package ch.briggen.bfh.sparklist.web;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import spark.Request;


class MitarbeiterWebHelper {
	
	
	private final static Logger log = LoggerFactory.getLogger(MitarbeiterWebHelper.class);
	
	public static Mitarbeiter MitarbeiterFromWeb(Request request)
	{
		return new Mitarbeiter(
				Long.parseLong(request.queryParams("MitarbeiterDetail.id")),
				request.queryParams("MitarbeiterDetail.funktion"),
				request.queryParams("MitarbeiterDetail.abteilung"),
				request.queryParams("MitarbeiterDetail.hierarchiestufe"),
				Integer.parseInt(request.queryParams("MitarbeiterDetail.lohnstufe")),
				request.queryParams("MitarbeiterDetail.name"),
				request.queryParams("MitarbeiterDetail.nachname"),
				request.queryParams("MitarbeiterDetail.adresse"),
				Integer.parseInt(request.queryParams("MitarbeiterDetail.plz")),
				request.queryParams("MitarbeiterDetail.ort"),
				request.queryParams("MitarbeiterDetail.mail"),
				Integer.parseInt(request.queryParams("MitarbeiterDetail.tel")));
	}

}
