package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * @author nathanael
 */

public class MitarbeiterUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterUpdateController.class);
		
	private MitarbeiterRepository MitarbeiterRepo = new MitarbeiterRepository();
	


	/**
	 * Schreibt den geänderten Mitarbeiter zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Mitarbeiter) mit der Mitarbeiter-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /Mitarbeiter/update
	 * 
	 * @return redirect nach /Mitarbeiter: via Browser wird /Mitarbeiter aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Mitarbeiter MitarbeiterDetail = MitarbeiterWebHelper.MitarbeiterFromWeb(request);
		
		log.trace("POST /Mitarbeiter/update mit MitarbeiterDetail " + MitarbeiterDetail);
		
		//Speichern des Mitarbeiters in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /Mitarbeiter&id=3 (wenn MitarbeiterDetail.getId == 3 war)
		MitarbeiterRepo.save(MitarbeiterDetail);
		response.redirect("/Mitarbeiterid="+MitarbeiterDetail.getId());
		return null;
	}
}


