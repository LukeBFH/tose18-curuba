package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * @author nathanael
 *
 */

public class RisikoDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(RisikoDeleteController.class);
		
	private RisikoRepository RisikoRepo = new RisikoRepository();

	/**
	 * Löscht das Risiko mit der übergebenen id in der Datenbank
	 * @return Redirect zurück zur Liste
	 */	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /Risiko/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		RisikoRepo.delete(longId);
		response.redirect("/Risiko");
		return null;
	}
}


