package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Risiko;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * @author nathanael
 */

public class StakeholderUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderUpdateController.class);
		
	private StakeholderRepository StakeholderRepo = new StakeholderRepository();
	


	/**
	 * Schreibt den geänderten Stakeholder zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/Stakeholder) mit der Stakeholder-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /Stakeholder/update
	 * 
	 * @return redirect nach /Stakeholder: via Browser wird /Stakeholder aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Stakeholder StakeholderDetail = StakeholderWebHelper.StakeholderFromWeb(request);
		
		log.trace("POST /Stakeholder/update mit StakeholderDetail " + StakeholderDetail);
		
		//Speichern des Stakeholders in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /Stakeholder&id=3 (wenn StakeholderDetail.getId == 3 war)
		StakeholderRepo.save(StakeholderDetail);
		response.redirect("/Stakeholderid="+StakeholderDetail.getId());
		return null;
	}
}


