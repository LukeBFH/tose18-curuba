package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Stakholdern
 * @author nathanael
 *
 */

public class StakeholderDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderDeleteController.class);
		
	private StakeholderRepository StakeholderRepo = new StakeholderRepository();

	/**
	 * Löscht den Stakeholder mit der übergebenen id in der Datenbank
	 * @return Redirect zurück zur Liste
	 */	
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /Stakeholder/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		StakeholderRepo.delete(longId);
		response.redirect("/Stakeholder");
		return null;
	}
}


