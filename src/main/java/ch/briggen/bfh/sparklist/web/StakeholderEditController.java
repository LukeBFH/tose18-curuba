package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import ch.briggen.bfh.sparklist.domain.Risiko;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import ch.briggen.bfh.sparklist.domain.StakeholderRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Stakeholder
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class StakeholderEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(StakeholderEditController.class);
	
	
	
	private StakeholderRepository StakeholderRepo = new StakeholderRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Stakeholders. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer Stakeholder erstellt (Aufruf von /Stakeholder/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars der Stakeholder mit der übergebenen id upgedated (Aufruf /Stakeholder/save)
	 * Hört auf GET /Stakeholder
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "StakeholderDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /Stakeholder für INSERT mit id " + idString);
			//der Submit-Button ruft /Stakeholder/new auf --> INSERT
			model.put("postAction", "/Stakeholder/new");
			model.put("StakeholderDetail", new Stakeholder());

		}
		else
		{
			log.trace("GET /Stakeholder für UPDATE mit id " + idString);
			//der Submit-Button ruft /Stakeholder/update auf --> UPDATE
			model.put("postAction", "/Stakeholder/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "StakeholderDetail" hinzugefügt. StakeholderDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Stakeholder i =StakeholderRepo.getById(id);
			model.put("StakeholderDetail", i);
		}
		
		//das Template StakeholderDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "StakeholderDetailTemplate");
	}
	
	
	
}


