package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import ch.briggen.bfh.sparklist.domain.Projekt;
import ch.briggen.bfh.sparklist.domain.ProjektRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Projekten
 * @author nathanael
 *
 */

public class ProjektEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(ProjektEditController.class);
	
	
	
	private ProjektRepository ProjektRepo = new ProjektRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Projekt. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Projekt erstellt (Aufruf von /Projekt/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Projekt mit der übergebenen id upgedated (Aufruf /Projekt/save)
	 * Hört auf GET /Projekt
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "ProjektDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /Projekt für INSERT mit id " + idString);
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/Projekt/new");
			model.put("ProjektDetail", new Projekt());

		}
		else
		{
			log.trace("GET /Projekt für UPDATE mit id " + idString);
			//der Submit-Button ruft /Projekt/update auf --> UPDATE
			model.put("postAction", "/Projekt/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "ProjektDetail" hinzugefügt. ProjektDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Projekt i =ProjektRepo.getById(id);
			model.put("ProjektDetail", i);
		}
		
		//das Template ProjektDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "ProjektDetailTemplate");
	}
	
	
	
}


