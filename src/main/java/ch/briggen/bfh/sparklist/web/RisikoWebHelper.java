package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Risiko;
import spark.Request;


class RisikoWebHelper {
	
	private final static Logger log = LoggerFactory.getLogger(RisikoWebHelper.class);
	
	public static Risiko RisikoFromWeb(Request request)
	{
		return new Risiko(
				Long.parseLong(request.queryParams("ProjektDetail.id")),
				request.queryParams("RisikoDetail.risikoart"),
				request.queryParams("RisikoDetail.beschreibung"),
				Integer.parseInt(request.queryParams("RisikoDetail.ewk")),
				Integer.parseInt(request.queryParams("RisikoDetail.asm")),
				request.queryParams("RisikoDetail.massnahmen"),
				request.queryParams("RisikoDetail.status"),
				request.queryParams("RisikoDetail.verantwortlich"));	
			
	}

}
