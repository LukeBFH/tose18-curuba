package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Mitarbeiter;
import ch.briggen.bfh.sparklist.domain.MitarbeiterRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Mitarbeiter
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class MitarbeiterEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterEditController.class);
	
	
	
	private MitarbeiterRepository MitarbeiterRepo = new MitarbeiterRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Risiko. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neuer Mitarbeiter erstellt (Aufruf von /Mitarbeiter/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars der Mitarbeiter mit der übergebenen id upgedated (Aufruf /Mitarbeiter/save)
	 * Hört auf GET /Mitarbeiter
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "MitarbeiterDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /Mitarbeiter für INSERT mit id " + idString);
			//der Submit-Button ruft /Mitarbeiter/new auf --> INSERT
			model.put("postAction", "/Mitarbeiter/new");
			model.put("ProjektDetail", new Mitarbeiter());

		}
		else
		{
			log.trace("GET /Mitarbeiter für UPDATE mit id " + idString);
			//der Submit-Button ruft /Mitarbeiter/update auf --> UPDATE
			model.put("postAction", "/Mitarbeiter/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "MitarbeiterDetail" hinzugefügt. MitarbeiterDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Mitarbeiter i =MitarbeiterRepo.getById(id);
			model.put("MitarbeiterDetail", i);
		}
		
		//das Template MitarbeiterDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "MitarbeiterDetailTemplate");
	}
	
	
	
}


