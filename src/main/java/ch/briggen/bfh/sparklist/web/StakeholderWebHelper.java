package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Stakeholder;
import spark.Request;


class StakeholderWebHelper {
	
	private final static Logger log = LoggerFactory.getLogger(StakeholderWebHelper.class);
	
	public static Stakeholder StakeholderFromWeb(Request request)
	{
		return new Stakeholder(
				Long.parseLong(request.queryParams("StakeholderDetail.id")),
				request.queryParams("StakeholderDetail.name"),
				request.queryParams("StakeholderDetail.vorname"),
				request.queryParams("StakeholderDetail.anforderung"),
				request.queryParams("StakeholderDetail.einfluss"),
				request.queryParams("StakeholderDetail.konfliktpotenzial"),
				request.queryParams("StakeholderDetail.rolle"));	
	}

}
