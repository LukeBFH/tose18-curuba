package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ch.briggen.bfh.sparklist.domain.Risiko;
import ch.briggen.bfh.sparklist.domain.RisikoRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Risiken
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class RisikoEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(RisikoEditController.class);
	
	
	
	private RisikoRepository RisikoRepo = new RisikoRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Risiko. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Risiko erstellt (Aufruf von /Risiko/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Risiko mit der übergebenen id upgedated (Aufruf /Risiko/save)
	 * Hört auf GET /Risiko
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "RisikoDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == idString)
		{
			log.trace("GET /Risiko für INSERT mit id " + idString);
			//der Submit-Button ruft /Risiko/new auf --> INSERT
			model.put("postAction", "/Risiko/new");
			model.put("ProjektDetail", new Risiko());

		}
		else
		{
			log.trace("GET /Risiko für UPDATE mit id " + idString);
			//der Submit-Button ruft /Risiko/update auf --> UPDATE
			model.put("postAction", "/Risiko/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "RisikoDetail" hinzugefügt. RisikoDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Risiko i =RisikoRepo.getById(id);
			model.put("RisikoDetail", i);
		}
		
		//das Template RisikoDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "RisikoDetailTemplate");
	}
	
	
	
}


