package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste Stakeholder
 * nathanael
 */
public class Stakeholder {
	
	private long id;
	private String name;
	private String vorname;
	private String anforderung;
	private String einfluss;
	private String konfliktpotenzial;
	private String rolle;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Stakeholder()
	{
		
	}

	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Stakeholders
	 * @param vorname Vorname des Stakeholders
	 * @param anforderung Anforderung des Stakeholders
	 * @param einfluss Einfluss des Stakeholders
	 * @param konflitkpotenzial Konfliktpotenzial des Stakeholders
	 * @param rolle Rolle des Stakeholders
	 */
	public Stakeholder(long id, String name, String vorname, String anforderung, String einfluss,
			String konfliktpotenzial, String rolle) {
		this.id = id;
		this.name = name;
		this.vorname = vorname;
		this.anforderung = anforderung;
		this.einfluss = einfluss;
		this.konfliktpotenzial = konfliktpotenzial;
		this.rolle = rolle;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getVorname() {
		return vorname;
	}


	public void setVorname(String vorname) {
		this.vorname = vorname;
	}


	public String getAnforderung() {
		return anforderung;
	}


	public void setAnforderung(String anforderung) {
		this.anforderung = anforderung;
	}


	public String getEinfluss() {
		return einfluss;
	}


	public void setEinfluss(String einfluss) {
		this.einfluss = einfluss;
	}


	public String getKonfliktpotenzial() {
		return konfliktpotenzial;
	}


	public void setKonfliktpotenzial(String konfliktpotenzial) {
		this.konfliktpotenzial = konfliktpotenzial;
	}


	public String getRolle() {
		return rolle;
	}


	public void setRolle(String rolle) {
		this.rolle = rolle;
	}


	@Override
	public String toString() {
		return "Stakeholder [id=" + id + ", name=" + name + ", vorname=" + vorname + ", anforderung=" + anforderung
				+ ", einfluss=" + einfluss + ", konfliktpotenzial=" + konfliktpotenzial + ", rolle=" + rolle + "]";
	}
	
	
	
	
	
}
