package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Stakeholder. 
 * Hier werden alle Funktionen für die DB-Operationen zu Stakeholder implementiert
 * @author nathanael
 *
 */


public class StakeholderRepository {
	
	private final Logger log = LoggerFactory.getLogger(StakeholderRepository.class);
	

	/**
	* Liefert alle Stakeholder in der DB
	* @return Collection aller Stakeholder
	*/
	public Collection<Stakeholder> getAll() {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Stakeholder");
			ResultSet rs = stmt.executeQuery();
			return mapStakeholder(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Stakeholder ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	* Liefert alle Stakeholder mit dem angegebenen Namen
	*/
	
	public Collection<Stakeholder> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Stakeholder where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapStakeholder(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Stakeholder by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	* Liefert das Stakeholder mit der übergebenen Id
	* @param id id vom Stakeholder
	* @return Stakeholder oder NULL
	*/
	public Stakeholder getById(long id) {
		log.trace("getById " + id);
		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Stakeholder where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapStakeholder(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Stakeholder by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	* Speichert den übergebenenen Stakeholder in der Datenbank. UPDATE.
	* @param i
	*/
	public void save(Stakeholder i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Stakeholder set name=?, vorname=?, anforderung=?, einfluss=?, konfliktpozenzial=?, rolle=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setString(3, i.getAnforderung());
			stmt.setString(4, i.getEinfluss());
			stmt.setString(5, i.getKonfliktpotenzial());
			stmt.setString(6, i.getRolle());
			stmt.setLong(7, i.getId());
			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Stakeholder " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	* Löscht das Stakeholder mit der angegebenen Id von der DB
	* @param id Stakeholder ID
	*/
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Stakeholder where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Stakeholder by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	* Speichert das angegebene Stakeholder in der DB. INSERT.
	* @param i neu zu erstellendes Stakeholder
	* @return Liefert die von der DB generierte id des neuen Stakeholder zurück
	*/
	public long insert(Stakeholder i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Stakeholder (name, vorname, anforderung, einfluss, konfliktpotenzial, rolle ) values (?,?,?,?,?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setString(3, i.getAnforderung());
			stmt.setString(3, i.getEinfluss());
			stmt.setString(3, i.getKonfliktpotenzial());
			stmt.setString(3, i.getRolle());
			
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Stakeholder " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	* Helper zum konvertieren der Resultsets in Stakeholder-Objekte. Siehe getByXXX Methoden.
	* @author nathanael
	* @throws SQLException 
	*
	*/
	private static Collection<Stakeholder> mapStakeholder(ResultSet rs) throws SQLException
	{
		LinkedList<Stakeholder> list = new LinkedList<Stakeholder>();
		while(rs.next())
		{
			Stakeholder i = new Stakeholder(rs.getLong("id"),rs.getString("name"),rs.getString("vorname"), rs.getString("anforderung"), rs.getString("einfluss"), rs.getString("konfliktpotenzial"), rs.getString("rolle"));
			list.add(i);
		}
		return list;
	}


}
