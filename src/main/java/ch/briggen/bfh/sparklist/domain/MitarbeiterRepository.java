package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Mitarbeiter. 
 * Hier werden alle Funktionen für die DB-Operationen zu Mitarbeiter implementiert
 * @author nathanael
 *
 */


public class MitarbeiterRepository {
	
	private final Logger log = LoggerFactory.getLogger(MitarbeiterRepository.class);
	

	/**
	 * Liefert alle Mitarbeiter in der DB
	 * @return Collection aller Mitarbeiter
	 */
	public Collection<Mitarbeiter> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Mitarbeiter");
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Mitarbeiter ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Mitarbeiter mit dem angegebenen Namen
	 */
	
	public Collection<Mitarbeiter> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Mitarbeiter where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Mitarbeiter by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert den Mitarbeiter mit der übergebenen Id
	 * @param id id vom Mitarbeiter
	 * @return Mitarbeiter oder NULL
	 */
	public Mitarbeiter getById(long id) {
		log.trace("getById " + id);
		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Mitarbeiter where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapMitarbeiter(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Mitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert den übergebenen Mitarbeiter in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Mitarbeiter i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Mitarbeiter set funktion=?, abteilung=?, hierarchiestufe=?, lohnstufe=?, name=?, nachname=?, adresse=?, plz=?, ort=?, mail=?, tel=? where id=?");
			stmt.setString(1, i.getFunktion());
			stmt.setString(2, i.getAbteilung());
			stmt.setString(3, i.getHierarchiestufe());
			stmt.setInt(4, i.getLohnstufe());
			stmt.setString(5, i.getName());
			stmt.setString(6, i.getNachname());
			stmt.setString(7, i.getAdresse());
			stmt.setInt(8, i.getPlz());
			stmt.setString(9, i.getOrt());
			stmt.setString(10, i.getMail());
			stmt.setInt(11, i.getTel());
		
			stmt.setLong(6, i.getId());
			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Mitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht den Mitarbeiter mit der angegebenen Id von der DB
	 * @param id Mitarbeiter ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Mitarbeiter where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Mitarbeiter by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert den angegebenen Mitarbeiter in der DB. INSERT.
	 * @param i neu zu erstellender Mitarbeiter
	 * @return Liefert die von der DB generierte id des neuen Mitarbeiter zurück
	 */
	public long insert(Mitarbeiter i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Mitarbeiter (funktion, abteilung, hierarchiestufe, lohnstufe, name, nachname, adresse, plz, ort, mail, tel) values (?,?,?,?,?,?,?,?,?,?,?)");
			stmt.setString(1, i.getFunktion());
			stmt.setString(2, i.getAbteilung());
			stmt.setString(3, i.getHierarchiestufe());
			stmt.setInt(4, i.getLohnstufe());
			stmt.setString(5, i.getName());
			stmt.setString(6, i.getNachname());
			stmt.setString(7, i.getAdresse());
			stmt.setInt(8, i.getPlz());
			stmt.setString(9, i.getOrt());
			stmt.setString(10, i.getMail());
			stmt.setInt(11, i.getTel());
			
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Mitarbeiter " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Mitarbeiter-Objekte. Siehe getByXXX Methoden.
	 * @author nathanael
	 * @throws SQLException 
	 *
	 */
	private static Collection<Mitarbeiter> mapMitarbeiter(ResultSet rs) throws SQLException 
	{
		LinkedList<Mitarbeiter> list = new LinkedList<Mitarbeiter>();
		while(rs.next())
		{
			Mitarbeiter i = new Mitarbeiter(rs.getLong("id"),rs.getString("funktion"),rs.getString("abteilung"), rs.getString("hierarchiestufe"), rs.getInt("lohnstufe"), rs.getString("name"), rs.getString("nachname"),  rs.getString("adresse"), rs.getInt("plz"), rs.getString("ort" ),  rs.getString("mail"),  rs.getInt("tel" ));
			list.add(i);
		}
		return list;
	}

}
