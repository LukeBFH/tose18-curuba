package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in Liste Projekte
 * @author nathanael
 *
 */

public class Mitarbeiter {
	
	private long id;
	private String funktion;
	private String abteilung;
	private String hierarchiestufe;
	private int lohnstufe;
	private String name;
	private String nachname;
	private String adresse;
	private int plz;
	private String ort;
	private String mail;
	private int tel;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Mitarbeiter()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param Funktion
	 * @param Abteilung
	 * @param Hierarchiestufe
	 * @param Lohnstufe
	 * @param name
	 * @param nachname
	 * @param adresse
	 * @param plz
	 * @param ort
	 * @param mail
	 * @param tel
	 */

	public Mitarbeiter(long id, String funktion, String abteilung, String hierarchiestufe, int lohnstufe, String name,
			String nachname, String adresse, int plz, String ort, String mail, int tel) {
		this.id = id;
		this.funktion = funktion;
		this.abteilung = abteilung;
		this.hierarchiestufe = hierarchiestufe;
		this.lohnstufe = lohnstufe;
		this.name = name;
		this.nachname = nachname;
		this.adresse = adresse;
		this.plz = plz;
		this.ort = ort;
		this.mail = mail;
		this.tel = tel;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFunktion() {
		return funktion;
	}

	public void setFunktion(String funktion) {
		this.funktion = funktion;
	}

	public String getAbteilung() {
		return abteilung;
	}

	public void setAbteilung(String abteilung) {
		this.abteilung = abteilung;
	}

	public String getHierarchiestufe() {
		return hierarchiestufe;
	}

	public void setHierarchiestufe(String hierarchiestufe) {
		this.hierarchiestufe = hierarchiestufe;
	}

	public int getLohnstufe() {
		return lohnstufe;
	}

	public void setLohnstufe(int lohnstufe) {
		this.lohnstufe = lohnstufe;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNachname() {
		return nachname;
	}

	public void setNachname(String nachname) {
		this.nachname = nachname;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getPlz() {
		return plz;
	}

	public void setPlz(int plz) {
		this.plz = plz;
	}

	public String getOrt() {
		return ort;
	}

	public void setOrt(String ort) {
		this.ort = ort;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public int getTel() {
		return tel;
	}

	public void setTel(int tel) {
		this.tel = tel;
	}

	@Override
	public String toString() {
		return "Mitarbeiter [id=" + id + ", funktion=" + funktion + ", abteilung=" + abteilung + ", hierarchiestufe="
				+ hierarchiestufe + ", lohnstufe=" + lohnstufe + ", name=" + name + ", nachname=" + nachname
				+ ", adresse=" + adresse + ", plz=" + plz + ", ort=" + ort + ", mail=" + mail + ", tel=" + tel + "]";
	}
	
	
	
	

	

	
	
	

}
