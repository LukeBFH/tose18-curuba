package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Stakeholder. 
 * Hier werden alle Funktionen für die DB-Operationen zu Stakeholder implementiert
 * @author nathanael
 *
 */


public class ProjektRepository {
	
	private final Logger log = LoggerFactory.getLogger(ProjektRepository.class);
	

	/**
	* Liefert alle Stakeholder in der DB
	* @return Collection aller Stakeholder
	*/
	public Collection<Projekt> getAll() {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Projekt");
			ResultSet rs = stmt.executeQuery();
			return mapProjekt(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Stakeholder ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	* Liefert alle Stakeholder mit dem angegebenen Namen
	*/
	
	public Collection<Projekt> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Projekt where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapProjekt(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Projekt by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	* Liefert das Projekt mit der übergebenen Id
	* @param id id vom Projekt
	* @return Projekt oder NULL
	*/
	public Projekt getById(long id) {
		log.trace("getById " + id);
		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Projekt where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapProjekt(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Projekt by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	* Speichert das übergebene Projekt in der Datenbank. UPDATE.
	* @param i
	*/
	public void save(Projekt i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Projekt set name=?, beschreibung=?, relevanz=?, KostenSOLL=?, KostenIST=? where id=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getRelevanz());
			stmt.setInt(4, i.getKostenSOLL());
			stmt.setInt(5, i.getKostenIST());
			stmt.setLong(6, i.getId());
			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Projekt " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	* Löscht das Projekt mit der angegebenen Id von der DB
	* @param id Projekt ID
	*/
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Projekt where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Projekt by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	* Speichert das angegebene Projekt in der DB. INSERT.
	* @param i neu zu erstellendes Projekt
	* @return Liefert die von der DB generierte id des neuen Projekt zurück
	*/
	public long insert(Projekt i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Projekt (name, beschreibung, relevanz, kostenSOLL, kostenIST ) values (?,?,?,?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getBeschreibung());
			stmt.setString(3, i.getRelevanz());
			stmt.setInt(4, i.getKostenSOLL());
			stmt.setInt(5, i.getKostenIST());
			
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Projekt " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	* Helper zum konvertieren der Resultsets in Stakeholder-Objekte. Siehe getByXXX Methoden.
	* @author nathanael
	* @throws SQLException 
	*
	*/
	private static Collection<Projekt> mapProjekt(ResultSet rs) throws SQLException{
	
		LinkedList<Projekt> list = new LinkedList<Projekt>();
		while(rs.next())
		{
			Projekt i = new Projekt(rs.getLong("id"),rs.getString("name"),rs.getString("beschreibung"), rs.getString("relevanz"), rs.getInt("kostenSOLL"), rs.getInt("kostenIST"));
			list.add(i);
		}
		return list;
	}

}

