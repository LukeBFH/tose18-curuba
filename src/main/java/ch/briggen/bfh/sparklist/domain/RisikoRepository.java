package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Risiken. 
 * Hier werden alle Funktionen für die DB-Operationen zu Projekte implementiert
 * @author nathanael
 *
 */


public class RisikoRepository {
	
	private final Logger log = LoggerFactory.getLogger(RisikoRepository.class);
	

	/**
	 * Liefert alle Risiken in der DB
	 * @return Collection aller Risiken
	 */
	public Collection<Risiko> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Risiko");
			ResultSet rs = stmt.executeQuery();
			return mapRisiko(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all Risiko ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Risiken mit dem angegebenen Namen
	 */
	
	public Collection<Risiko> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Risiko where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapRisiko(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Risiko by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Risiko mit der übergebenen Id
	 * @param id id vom Risiko
	 * @return Risiko oder NULL
	 */
	public Risiko getById(long id) {
		log.trace("getById " + id);
		
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select * from Risiko where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapRisiko(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving Risiko by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene Risiko in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Risiko i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update Risiko set risikoart=?, beschreibung=?, ewk=?, asm=?, massnahmen=?, status=?, verantwortlich=?, where id=?");
			stmt.setString(1, i.getRisikoart());
			stmt.setString(2, i.getBeschreibung());
			stmt.setInt(3, i.getEwk());
			stmt.setInt(4, i.getAsm());	
			stmt.setString(5, i.getMassnahmen());
			stmt.setString(6, i.getStatus());
			stmt.setString(7, i.getVerantwortlich());
			stmt.setLong(7, i.getId());
			
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Risiko " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Risiko mit der angegebenen Id von der DB
	 * @param id Risiko ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from Risiko where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing Risiko by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Risiko in der DB. INSERT.
	 * @param i neu zu erstellendes Risiko
	 * @return Liefert die von der DB generierte id des neuen Risiko zurück
	 */
	public long insert(Risiko i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into Risiko (risikoart, beschreibung, ewk, asm, massnahmen, status, verantwortlich ) values (?,?,?,?,?,?,?)");
			stmt.setString(1, i.getRisikoart());
			stmt.setString(2, i.getBeschreibung());
			stmt.setInt(3, i.getEwk());
			stmt.setInt(4, i.getAsm());	
			stmt.setString(5, i.getMassnahmen());
			stmt.setString(6, i.getStatus());
			stmt.setString(7, i.getVerantwortlich());
			
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating Risiko " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Risiko-Objekte. Siehe getByXXX Methoden.
	 * @author nathanael
	 * @throws SQLException 
	 *
	 */
	private static Collection<Risiko> mapRisiko(ResultSet rs) throws SQLException 
	{
		LinkedList<Risiko> list = new LinkedList<Risiko>();
		while(rs.next())
		{
			Risiko i = new Risiko(rs.getLong("id"),rs.getString("risikoart"),rs.getString("beschreibung"), rs.getInt("ewk"), rs.getInt("asm"), rs.getString("massnahmen"), rs.getString("status"),rs.getString("verantwortlich"));
			list.add(i);
		}
		return list;
	}

}
