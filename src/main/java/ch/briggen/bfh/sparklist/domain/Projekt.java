package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in Liste Projekte
 * @author nathanael
 *
 */

public class Projekt {
	
	private long id;
	private String name;
	private String beschreibung;
	private String relevanz;
	private int kostenSOLL;
	private int kostenIST;
	
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Projekt()
	{
		
	}
	
	/**
	 * Konstruktor
	 * @param id Eindeutige Id
	 * @param name Name des Projekts
	 * @param relevanz Strategische Relevanz vom Projekt
	 * @param kostenSoll geplante Kosten
	 * @param kostenIST effektive Kosten
	 */
	
	public Projekt(long id, String name, String beschreibung, String relevanz, int kostenSOLL, int kostenIST) {
		this.id = id;
		this.name = name;
		this.beschreibung = beschreibung;
		this.relevanz = relevanz;
		this.kostenSOLL = kostenSOLL;
		this.kostenIST = kostenIST;
	}
	

	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getRelevanz() {
		return relevanz;
	}

	public void setRelevanz(String relevanz) {
		this.relevanz = relevanz;
	}

	public int getKostenSOLL() {
		return kostenSOLL;
	}

	public void setKostenSOLL(int kostenSOLL) {
		this.kostenSOLL = kostenSOLL;
	}

	public int getKostenIST() {
		return kostenIST;
	}

	public void setKostenIST(int kostenIST) {
		this.kostenIST = kostenIST;
	}

	@Override
	public String toString() {
		return "Projekt [id=" + id + ", name=" + name + ", beschreibung=" + beschreibung + ", relevanz=" + relevanz
				+ ", kostenSOLL=" + kostenSOLL + ", kostenIST=" + kostenIST + "]";
	}

}
