package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in Liste Risiko
 * @author nathanael
 *
 */

public class Risiko {
	
	private long id;
	private String risikoart;
	private String beschreibung;
	private int ewk;
	private int asm;
	private String massnahmen;
	private String status;
	private String verantwortlich;
	
	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Risiko()
	{
		
	}
	
	/**
	 * Konstruktorx
	 * @param id Eindeutige Id
	 * @param risikoart Was für eine Risikokategorie
	 * @param beschreibung Beschreibung des Risikos
	 * @param ewk Eintrittswahrscheinlichkeit des Risikos 
	 * @param asm Ausmass des Riskos
	 * @param massnahmen Massnahmen gegen Risiko
	 * @param status Status des Riskos
	 * @param verantwortlich verantwortlicher des Risikos
	 */
	

	public Risiko(long id, String risikoart, String beschreibung, int ewk, int asm, String massnahmen, String status,
			String verantwortlich) {
	
		this.id = id;
		this.risikoart = risikoart;
		this.beschreibung = beschreibung;
		this.ewk = ewk;
		this.asm = asm;
		this.massnahmen = massnahmen;
		this.status = status;
		this.verantwortlich = verantwortlich;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRisikoart() {
		return risikoart;
	}

	public void setRisikoart(String risikoart) {
		this.risikoart = risikoart;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public int getEwk() {
		return ewk;
	}

	public void setEwk(int ewk) {
		this.ewk = ewk;
	}

	public int getAsm() {
		return asm;
	}

	public void setAsm(int asm) {
		this.asm = asm;
	}

	public String getMassnahmen() {
		return massnahmen;
	}

	public void setMassnahmen(String massnahmen) {
		this.massnahmen = massnahmen;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVerantwortlich() {
		return verantwortlich;
	}

	public void setVerantwortlich(String verantwortlich) {
		this.verantwortlich = verantwortlich;
	}

	@Override
	public String toString() {
		return "Risiko [id=" + id + ", risikoart=" + risikoart + ", beschreibung=" + beschreibung + ", ewk=" + ewk
				+ ", asm=" + asm + ", massnahmen=" + massnahmen + ", status=" + status + ", verantwortlich="
				+ verantwortlich + "]";
	}
	
	
	

}
